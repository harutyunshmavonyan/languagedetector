import string
import sys

def addTitleToLine(line, labelName):
	newLine = "" + "__label__" + labelName + " " + line
	return newLine

def removePunctuationAndNumbers(line):
	return "".join((char for char in line if ((char == "_") or (not char in string.punctuation) or (not char.isdigit()))))				

doc = sys.argv[1]
with open(doc) as data:
	with open(doc + ".preprocessed", 'w') as preprocessed:
		for line in data:
			line = addTitleToLine(line, sys.argv[2])
			line = line.lower()
			line = removePunctuationAndNumbers(line)
			preprocessed.write(line)
