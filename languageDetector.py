import json
import sys
import string
import operator

def removePunctuation(line):
    return "".join((char for char in line if (char == "_" or not char in string.punctuation)))

def removeIntersection(dict1, dict2):
	words1 = dict1.keys()
	words2 = dict2.keys()
	intersection = set(words1) & set(words2)
	
	for word in intersection:
		dict1.pop(word, None)
		dict2.pop(word, None)
	return dict1, dict2

def readData():
	dictionaries = []
	for doc in [sys.argv[2], sys.argv[3]]:
		wordsDictionary = {}
		with open(doc, 'r') as doc:
			for line in doc:
				line = line.lower()
				line = removePunctuation(line)
				for word in line.split():
					if wordsDictionary.has_key(word):
						wordsDictionary[word] += 1
					else:
						wordsDictionary[word] = 1
		dictionaries.append(wordsDictionary)
	return dictionaries

def deseserializeJson(jsonFilePath):
	with open(jsonFilePath, 'r') as jsonFile:
		jsonString = jsonFile.read()
		jsonData = json.loads(jsonString)
	return jsonData

def predictForLine(line, lang1Dict, lang2Dict):
	line = line.lower()
	line = removePunctuation(line)
	lang1Score, lang2Score = 0, 0
	for word in line.split():
		if lang1Dict.has_key(word):
			lang1Score += 1
		elif lang2Dict.has_key(word):
			lang2Score += 1
	#print float(lang1Score) / (lang1Score + lang2Score), float(lang2Score) / (lang1Score + lang2Score)
	return int(lang2Score >= lang1Score)

if sys.argv[1] == "train":
	dict1, dict2 = readData()
	dict1, dict2 = removeIntersection(dict1, dict2)
	for dictionary, sysArgNumber in zip([dict1, dict2], [2, 3]):
		jsonString = json.dumps(dictionary)
		with open(sys.argv[sysArgNumber] + ".json", 'w') as jsonFile:
			jsonFile.write(jsonString)
elif sys.argv[1] == "test":
	lang1File = open(sys.argv[2])
	lang2File = open(sys.argv[3])
	lang1Dict = deseserializeJson(sys.argv[4])
	lang2Dict = deseserializeJson(sys.argv[5])
	trueResult, falseResult = 0, 0
	for lang1Line in lang1File:
		if predictForLine(lang1Line, lang1Dict, lang2Dict) == 0:
			trueResult += 1
		else:
			falseResult += 1
	for lang2Line in lang2File:
		if predictForLine(lang2Line, lang1Dict, lang2Dict) == 1:
			trueResult += 1
		else:
			falseResult += 1
	print "Accuracy: " + str(float(trueResult) / (trueResult + falseResult))
